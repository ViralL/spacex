import React, { Fragment } from 'react';
import Routes from './routes';
import Header from './components/Header';
import Footer from './components/Footer';
import Popup from './components/Popup';
import './styles/app.scss';

const App = () => {
    return (
        <Fragment>
            <Header />
            <main className="main">
                {Routes}
            </main>
            <Footer />
            <Popup />
        </Fragment>
    );
};

export default App;
