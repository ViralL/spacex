import React from 'react';
import { Link } from 'react-router-dom';

const Header = () =>
    <header className="header">
        <div className="container text-medium">
            <Link to="/">SpaceX App</Link>
        </div>
    </header>;

export default Header;
