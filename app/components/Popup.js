import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { resetPopupState } from '../actions/popupActions';
import { getNoData } from '../utils/utils';

class Popup extends Component {

    handleClose = () => {
        this.props.actions.resetPopupState();
        document.body.classList.remove('fixed');
    };
    getColumns(item) {
        return Object.keys(item).map((key, index) => {
            if (typeof item[key] === 'object' && item[key] !== null && item[key].length !== 0) {
                return this.getColumns(item[key]);
            }
            return (
                <div className="row text-medium" key={index}>
                    <div className="col-xs-12 col-md-4">
                        {key.charAt(0).toUpperCase() + key.slice(1).replace(/_/g, ' ')} :
                    </div>
                    <div className="col-xs-12 col-md-4">
                        {getNoData(item[key])}
                    </div>
                </div>
            );
        });
    }

    render() {
        const { popup } = this.props;
        return (
            popup.active && (
                <div className="popup">
                    <div className="popup-header">
                        <h1 className="text-big">{popup.title}</h1>
                        <button
                            type="button"
                            className="btn-close"
                            onClick={this.handleClose}
                        >
                            X
                        </button>
                    </div>
                    <div className="popup-body">
                        {!popup.payload ? (
                            this.getColumns(popup.description)
                        ) : (
                            this.getColumns(popup.description.payloads[0])
                        )}
                    </div>
                </div>
            )
        );
    }
}

Popup.propTypes = {
    description: PropTypes.object,
    handleClose: PropTypes.func,
    actions: PropTypes.object,
    popup: PropTypes.object
};

Popup.defaultProps = {
};

function mapStateToProps(state) {
    return {
        popup: state.popup
    };
}
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                resetPopupState
            },
            dispatch
        )
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Popup);
