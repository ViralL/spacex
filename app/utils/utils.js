import React from 'react';

function getNoData(item) {
    let string;
    if (item === null || item.length === 0) {
        string = 'No data';
    } else {
        if (typeof item === 'boolean') {
            string = item.toString();
        } else {
            string = item;
        }
    }
    return string;
}

function getLinks(item) {
    let string;
    if (item === null || item.length === 0) {
        string = 'No link';
    } else {
        string = <a href={item} target="_blank">Link</a>;
    }
    return string;
}

function getLessText(item) {
    getNoData(item);
    if (item !== null) {
        let sliced = item.slice(0, 50);
        if (sliced.length < item.length) {
            sliced += '...';
        }
        return sliced;
    }
    return item;
}

export { getNoData, getLessText, getLinks };
