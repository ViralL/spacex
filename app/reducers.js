import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import popup from './reducers/popup';
import pastLaunches from './reducers/past';
import upcomingLaunches from './reducers/upcoming';

const rootReducer = combineReducers({
    upcomingLaunches,
    pastLaunches,
    popup,
    routing
});

export default rootReducer;
