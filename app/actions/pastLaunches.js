import * as types from './types';
import axios from 'axios';

export function fetchPastLaunches() {
    return function(dispatch) {
        dispatch({
            type: types.PAST_DATA_REQUEST,
            value: {
                isLoading: true,
                isError: false
            }
        });
        axios
            .get('https://api.spacexdata.com/v2/launches')
            .then(({ data }) => {
                dispatch({
                    type: types.PAST_DATA_SUCCESS,
                    value: {
                        data,
                        isLoading: false,
                        isError: false
                    }
                });
            })
            .catch(() => {
                dispatch({
                    type: types.PAST_DATA_ERROR,
                    value: {
                        isLoading: false,
                        isError: true
                    }
                });
            });
    };
}
