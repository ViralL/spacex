import * as types from './types';

export function triggerPopupState(title, description, payload) {
    return {
        type: types.TRIGGER_POPUP_STATE,
        value: {
            title,
            description,
            payload
        }
    };
}

export function resetPopupState() {
    return {
        type: types.RESET_POPUP_STATE
    };
}
