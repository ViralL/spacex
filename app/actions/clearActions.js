import * as types from './types';

export function resetData() {
    return {
        type: types.CLEAR_DATA
    };
}
