import * as types from './types';
import axios from 'axios';

export function fetchUcomingLaunches() {
    return function(dispatch) {
        dispatch({
            type: types.UPCOMING_DATA_REQUEST,
            value: {
                isLoading: true,
                isError: false
            }
        });
        axios
            .get('https://api.spacexdata.com/v2/launches/upcoming')
            .then(({ data }) => {
                dispatch({
                    type: types.UPCOMING_DATA_SUCCESS,
                    value: {
                        data,
                        isLoading: false,
                        isError: false
                    }
                });
            })
            .catch(() => {
                dispatch({
                    type: types.UPCOMING_DATA_ERROR,
                    value: {
                        isLoading: false,
                        isError: true
                    }
                });
            });
    };
}
