const reducer = (state = {}, action) => {
    switch (action.type) {
        case 'PAST_DATA_REQUEST':
            return action.value;
        case 'PAST_DATA_SUCCESS':
            return action.value;
        case 'CLEAR_DATA':
            return {};
        default:
            return state;
    }
};

export default reducer;
