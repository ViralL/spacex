export const initialState = {
    popup: {
        payload: false,
        active: false,
        title: '',
        description: []
    }
};

const reducer = (state = initialState.popup, action) => {
    switch (action.type) {
        case 'RESET_POPUP_STATE':
            return {
                ...state,
                active: false
            };
        case 'TRIGGER_POPUP_STATE':
            return {
                ...state,
                ...action.value,
                active: true
            };
        default:
            return state;
    }
};

export default reducer;
