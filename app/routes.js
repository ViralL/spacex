import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Index from './containers/Index';
import Past from './containers/PastData';
import Upcoming from './containers/UpcomingData';

export default (
	<Switch>
		<Route exact path="/" component={Index} />
		<Route path="/past" component={Past} />
		<Route path="/upcoming" component={Upcoming} />
	</Switch>
);
