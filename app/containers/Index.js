import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { resetData } from '../actions/clearActions';

class Index extends Component {

    componentWillMount() {
        this.props.actions.resetData();
    }
    render() {
        return (
            <div className="section background-dark section--lg">
                <div className="container text-center">
                    <h3 className="text-huge text-white text-with-subtitle">Go throw the flow</h3>
                    <br />
                    <div className="text-big text-gray row">
                        <div className="col-md-6 col-xs-12">
                            <Link to="/past" className="text-success">check your <b>Past Launches</b></Link>
                        </div>
                        <div className="col-md-6 col-xs-12">
                            <Link to="/upcoming" className="text-success">check your <b>Upcoming Launches</b></Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Index.propTypes = {
    actions: PropTypes.any
};

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                resetData
            },
            dispatch
        )
    };
}

export default connect(null, mapDispatchToProps)(Index);
