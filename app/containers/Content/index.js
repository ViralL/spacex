import React, { Component } from 'react';
import PropTypes from 'prop-types';

// redux
import { getNoData, getLessText, getLinks } from '../../utils/utils';
import Presentation from './presentation';
import Loader from '../../components/Loader';

class Content extends Component {
    state = {
        filterValue: {}
    };

    componentWillMount() {
        this.props.actions.getContent();
    }

    getColumns(item) {
        return Object.keys(item)
            .map((key, index) => {
                if (typeof item[key] === 'object' && item[key] !== null && key !== 'first_stage' && key !== 'second_stage' && item[key].length !== 0) {
                    return this.getColumns(item[key]);
                }
                return (
                    <th key={index}>
                        {key.charAt(0).toUpperCase() + key.slice(1).replace(/_/g, ' ')}
                        <div className="input">
                            <input id={index} onChange={this.handleChange.bind(this, key)} placeholder="" type="text" />
                        </div>
                    </th>
                );
            });
    }

    handleClick(title, desc, payload) {
        document.body.classList.add('fixed');
        this.props.actions.triggerPopupState(title, desc, payload);
    }

    handleChange(filterKey, event) {
        const filterValue = event.target.value.toLowerCase().trim();
        this.setState({
            filterValue: {
                ...this.state.filterValue,
                [filterKey]: filterValue
            }
        });
        if (filterValue.length === 0) {
            this.setState({
                filterValue: {}
            });
        }
    }

    getData() {
        const keys = Object.keys(this.state.filterValue);
        if (keys.length) {
            return this.props.content
                .filter(item => {
                    return keys.some((key) => {
                        return item[key].toString().toLowerCase().includes(this.state.filterValue[key]);
                    });
                });
        }
        return this.props.content;
    }

    render() {
        return this.props.content && !this.props.isLoading
            ? (
                <Presentation
                    isError={this.props.isError}
                    isUpcoming={this.props.isUpcoming}
                    content={this.getData()}
                    getLinks={getLinks.bind(this)}
                    getNoData={getNoData.bind(this)}
                    getLessText={getLessText.bind(this)}
                    ifCLicked={this.handleClick.bind(this)}
                    getColumns={this.getColumns(this.props.content[0])}
                />
            )
            : (
                <Loader />
            );
    }
}

Content.propTypes = {
    actions: PropTypes.any,
    isLoading: PropTypes.bool,
    isError: PropTypes.bool,
    isUpcoming: PropTypes.bool,
    content: PropTypes.array
};

export default Content;
