import React from 'react';
import PropTypes from 'prop-types';

const Content = (props) => {
    return (
        <div className="container">
            {props.isError === true && (
                <div className="text-center text-huge">No data</div>
            )}
            <div className="table-responsible">
                <table className="table">
                    <thead>
                    <tr className="align-left">
                        {props.getColumns}
                    </tr>
                    </thead>
                    <tbody>
                    {props.content.map((item, index) => {
                        return (
                            <tr key={index}>
                                <td>{props.getNoData(item.flight_number)}</td>
                                <td>{props.getNoData(item.mission_name)}</td>
                                <td>{props.getNoData(item.mission_id)}</td>
                                {!props.isUpcoming && (<td>{props.getNoData(item.upcoming)}</td>)}
                                <td>{props.getNoData(item.launch_year)}</td>
                                <td>{props.getNoData(item.launch_date_unix)}</td>
                                <td>{props.getNoData(item.launch_date_utc)}</td>
                                <td>{props.getNoData(item.launch_date_local)}</td>
                                <td>{props.getNoData(item.is_tentative)}</td>
                                <td>{props.getNoData(item.tentative_max_precision)}</td>
                                <td>{props.getNoData(item.rocket.rocket_id)}</td>
                                <td>{props.getNoData(item.rocket.rocket_name)}</td>
                                <td>{props.getNoData(item.rocket.rocket_type)}</td>
                                <td>
                                    <span
                                        className="link"
                                        onClick={()=>{props.ifCLicked('First stage', item.rocket.first_stage.cores[0], false);}}
                                    >
                                        More &oplus;
                                    </span>
                                </td>
                                <td>
                                    <span
                                        className="link"
                                        onClick={()=>{props.ifCLicked('Second stage', item.rocket.second_stage, true);}}
                                    >
                                        More &oplus;
                                    </span>
                                </td>
                                <td>{props.getNoData(item.rocket.fairings ? item.rocket.fairings.reused : 'No data')}</td>
                                <td>{props.getNoData(item.rocket.fairings ? item.rocket.fairings.recovery_attempt : 'No data')}</td>
                                <td>{props.getNoData(item.rocket.fairings ? item.rocket.fairings.recovered : 'No data')}</td>
                                <td>{props.getNoData(item.rocket.fairings ? item.rocket.fairings.ship : 'No data')}</td>
                                <td>{props.getNoData(item.ships)}</td>
                                <td>{props.getNoData(item.telemetry.flight_club)}</td>
                                <td>{props.getNoData(item.reuse.core)}</td>
                                <td>{props.getNoData(item.reuse.side_core1)}</td>
                                <td>{props.getNoData(item.reuse.side_core2)}</td>
                                <td>{props.getNoData(item.reuse.fairings)}</td>
                                <td>{props.getNoData(item.reuse.capsule)}</td>
                                <td>{props.getNoData(item.launch_site.site_id)}</td>
                                <td>{props.getNoData(item.launch_site.site_name)}</td>
                                <td>{props.getNoData(item.launch_site.site_name_long)}</td>
                                <td>{props.getNoData(item.launch_success)}</td>
                                <td>{props.getLinks(item.links.mission_patch)}</td>
                                <td>{props.getLinks(item.links.mission_patch_small)}</td>
                                {props.isUpcoming && (<td>{props.getLinks(item.links.reddit_campaign)}</td>)}
                                {props.isUpcoming && (<td>{props.getLinks(item.links.reddit_launch)}</td>)}
                                {props.isUpcoming && (<td>{props.getLinks(item.links.reddit_recovery)}</td>)}
                                {props.isUpcoming && (<td>{props.getLinks(item.links.reddit_media)}</td>)}
                                {props.isUpcoming && (<td>{props.getLinks(item.links.presskit)}</td>)}
                                <td>{props.getLinks(item.links.article_link)}</td>
                                <td>{props.getLinks(item.links.wikipedia)}</td>
                                <td>{props.getLinks(item.links.video_link)}</td>
                                <td>{props.getLinks(item.links.flickr_images)}</td>
                                <td>{props.getLessText(item.details)}</td>
                                {props.isUpcoming && (<td>{props.getNoData(item.upcoming)}</td>)}
                                <td>{props.getNoData(item.static_fire_date_utc)}</td>
                                <td>{props.getNoData(item.static_fire_date_unix)}</td>
                            </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

Content.propTypes = {
    isError: PropTypes.bool,
    isLoading: PropTypes.bool,
    isUpcoming: PropTypes.bool,
    content: PropTypes.array,
    getColumns: PropTypes.array,
    getNoData: PropTypes.func,
    ifCLicked: PropTypes.func,
    getLinks: PropTypes.func,
    getLessText: PropTypes.func
};

export default Content;
