// redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchUcomingLaunches } from '../actions/upcomingLaunches';
import { triggerPopupState } from '../actions/popupActions';

// components
import Content from '../containers/Content';

const mapStateToProps = (state) => {
    return {
        content: state.upcomingLaunches.data,
        isLoading: state.upcomingLaunches.isLoading,
        isError: state.upcomingLaunches.isError,
        isUpcoming: true
    };
};

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                getContent: fetchUcomingLaunches,
                triggerPopupState
            },
            dispatch
        )
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Content);
