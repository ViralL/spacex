// redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPastLaunches } from '../actions/pastLaunches';
import { triggerPopupState } from '../actions/popupActions';

// components
import Content from '../containers/Content';

const mapStateToProps = (state) => {
    return {
        content: state.pastLaunches.data,
        isLoading: state.pastLaunches.isLoading,
        isError: state.pastLaunches.isError,
        isUpcoming: false
    };
};

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(
            {
                getContent: fetchPastLaunches,
                triggerPopupState
            },
            dispatch
        )
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Content);
